
const app = getApp();
var onfire=require('../../utils/onfire1.0.6.js')
var onfiredata;
const db = wx.cloud.database();
Component({
  data:{
    ColorList: app.globalData.ColorList,
    color:'red',
    value: {
      Speed: 43,
      Power: 70,
      longitude:113.3245211,
      latitude:23.10229,
      F_value:0,
    },
    markers: [{
      iconPath: "../../images/Marker-select.png",
      id: 0,
      latitude:23.10229,
      longitude: 113.3245211,
      width: 50,
      height: 50
    }],
    polyline: [{
      points: [{
        longitude: 113.3245211,
        latitude: 23.10229
      }, {
        longitude: 113.324520,
        latitude: 23.21229
      }],
      color:"#FF0000DD",
      width: 2,
      dottedLine: true
    }],

  },

  ColorList: app.globalData.ColorList,
  color:'red',//进度条相关
  options: {
    addGlobalClass: true,
  },
  lifetimes:{
    attached: function () {
      var that=this;
      onfiredata=onfire.on("topic",function(parm1,parm2){
        console.log("parm1"+parm1)
        var jsonObj=JSON.parse(parm1);
        that.setData({
          'value.Speed': jsonObj.items.humidity.value,
          'value.Power': jsonObj.items.temp.value,
          'value.latitude': jsonObj.items.GeoLocation.value.Latitude,
          'value.longitude': jsonObj.items.GeoLocation.value.Longitude,
          'markers[0].latitude': jsonObj.items.GeoLocation.value.Latitude,
          'markers[0].longitude': jsonObj.items.GeoLocation.value.Longitude,
        })
    })
  },
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  SetColor(e) {
    this.setData({
      color: e.currentTarget.dataset.color,
      modalName: null
    })
  },
  SetActive(e) {
    this.setData({
      active: e.detail.value
    })
  },
  methods:{
    regionchange(e) {
      console.log(e.type)
    },
    markertap(e) {
      console.log(e.detail.markerId)
    },
    controltap(e) {
      console.log(e.detail.controlId)
    },
    
  }
  

})