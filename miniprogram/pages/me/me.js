var db = wx.cloud.database();
var app = getApp();
Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    avatarUrl: '',
    nickName: '',
    score: '',
    order: '',
    github: 'https://github.com/gritJack/Wapp'
  },
  pageLifetimes: {
    show() {
      if (typeof this.getTabBar === 'function' && this.getTabBar()) {
        this.getTabBar().setData({
          selected: 3
        })
      }
    }
  },
  methods:{
    goAbout: function() {
      wx.navigateTo({
        url: '../about/about',
      })
  
    },
  
    feedback: function() {
      wx.navigateTo({
        url: '../feedback/feedback',
      })
    },
  
    clickGit: function() {
      wx.setClipboardData({
        data: this.data.github,
        success: res => {
          wx.showModal({
            title: '已复制github地址',
            content: '如有需要欢迎访问本程序Github项目！',
          })
        },
      })
    },
  },

  
})